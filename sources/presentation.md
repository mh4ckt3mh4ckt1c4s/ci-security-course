# Before starting

- I am Quentin MICHAUD, engineer freshly graduated from Télécom SudParis and continuing as a PhD student in cybersecurity
- Please interrupt me if you did not understand something / you have a question
- This course^[https://gitlab.com/mh4ckt3mh4ckt1c4s/ci-security-course] and the related example project^[https://gitlab.com/mh4ckt3mh4ckt1c4s/gitlab-ci-rust-example] are both open (source) and available on GitLab

# What is CI?

## Continuous Integration

Continuous Integration (CI) is a developing practice where developers **integrate** changes into the main codebase as much as possible. The researched goal of frequent merges is to reduce as much as possible breaking changes and complex merging conflicts.

Today, CI is effectively applied by building and testing the codebase at each commit, at each merge and at each release.

## Uses of CI

As of today, the term "CI" is not only applied to testing code for frequent merges, but for most tasks that can be automated to improve the code. It can be license checks, style formatting, spelling, security scans, fuzzing...

It is important to decide what is important to run and when to run it, because big CIs can result in significant computing use and time. For example, the CI of the Firefox browser is composed of more than 5000 tasks, and a whole run takes more than 1500 hours. The Firefox project is consuming around 100 to 300 machine years per month!

## How to do CI in a project?

In order to use CI in a project, you will need:

- A CI server: it is the server on which you will build and test your codebase.
- A CI tool: this is the software that will be used to trigger the building and testing you want on events you choose. 

## Overview of CI tools

There are a lot of CI tools available, each with their own set of ups and downs. Here are a few of them, divided:

- **Jenkins** is one of the oldest and most known of the lot. It allows to automate all sort of tasks.
- Version Control System (VCS)-backed CI tools, such as **GitHub Actions**, **GitLab CI**, or **BitBucket Pipelines**. They are the most straightforward tools to use when using the corresponding VCS.
- Cloud provider-backed tools such as **AWS CodePipeline** or **Azure Pipelines**. This is an interesting possibility when one is already hosting services at the same provider. 

## How to choose a CI tool?

It can be difficult to choose the right tool for your project, but there are the most common criteria:

- The VCS you are using : you will not be able to use tools that does not support your VCS (e.g. Firefox is using Mercurial).
- Hosted VS cloud : depending on the requirements of your project (scalability, confidentiality...) you may need to choose a solution that you can host yourself, or use a cloud one.
- Adaptability and modularity : if you need specific tasks to be dealt with, you need to check if it is supported by the tool you want, and if not, will you be able to use a plugin or modify the tool source code to achieve it?

## Focusing on GitLab CI (and Rust for demos)

In this course, we will be focusing on **GitLab CI**, the CI tool integrated with GitLab. GitLab is one of the most used platform for software development and is used in various courses at school. Also, its CI tool is very complete and flexible.

In the demos, I will use a **Rust** project, as its ecosystem allows to do most tasks interesting in a CI quite easily using the `cargo` binary.  

## Some vocabulary

- The unit of a CI task in GitLab is called a **Job**. 
- A whole CI workload is a **Pipeline**. It is composed of at least one job, and can give restrictions on these jobs (e.g. do the compile job before the testing job)
- The computing component that allows GitLab to run CI workloads is called a **runner**. A basic runner can only execute one job at a time, so you need to have an available runner to execute a job. Multiple runners allow you to parallelize job execution, according to the policy set in the pipeline.

# CI for security...

## An example of why testing is essential

On 28th of August 2023, UK air traffic control operator main software crashed. No more planes was able to fly over UK : 2000 flights cancelled, £100 million lost.

The bug? The software was processing a list of waypoints from a flight plan, and did not expect to find the same element twice in the list, so crashed.

The system was running for 5 years and processed 15 million flights without problems.

The full story is [here](https://jameshaydon.github.io/nats-fail/).

---

![meme](bugmeme.jpg)

## Introduction

As we discussed before, we can use a CI for a lot of different actions, but we will focus here on tools that allows to **increase the quality of the software**, and continue with **security analysis tools**.

## Formatting

Before even looking at what the code do, one good practice is to **enforce a uniform code format**. This allows for a cleaner codebase but also for an easier reading and comprehension, especially when looking for bugs.

For this, specific tools exist for each language that can be configured to tell if a code is following the required format or not and what is the problem.

## Linting

Linting means **analyzing the source code** further than just the formatting, looking at potential bugs, deprecated functions, best practices, etc. 

Again, specific tools exist for each language and can be configured to match a project specific requirements. 

## Testing

It is very good practice to **write tests alongside the code** you add to a project. There are multiple test formats existing and the tools for executing the tests and reporting the results also changes for each language.

## Fuzzing

Fuzzing can be seen as a sort of testing, but it is different enough that is deserves its own part. Fuzzing consists of **throwing random input to the program** to see if we can make it crash or reach unexpected behavior.

There are different kinds of fuzzing:

::: nonincremental
- basic fuzzing in black box, that is not language-dependent,
- more complex white-box fuzzing that will analyze the source code and try to adapt the fuzzing input to explore all the source code.
:::

## SAST

Static Application Security Testing (SAST) is the practice of using a tool that will **scan the source code, or the resulting binary, for security vulnerabilities**. The software used depends generally on the programming language.

GitLab has built-in SAST support for various languages and files^[https://docs.gitlab.com/ee/user/application_security/sast/] (sadly no Rust yet^[https://gitlab.com/gitlab-org/gitlab/-/issues/390733]).

## DAST

Dynamic Application Security Testing (DAST) means **testing the application dynamically**, trying to interact realistically (and maliciously) with its various interfaces and APIs. DAST is mainly used for Web or user-facing applications. 

A well-known and widely used dynamic web application scanner is ZAP^[https://www.zaproxy.org/].

## Artifact scanning

When you build an application, you may want to redistribute it under various formats (Linux distribution packages, Windows installer, container...). You must **ensure that the package you put around the application is not vulnerable** in some way. 

This is why several tools exists to scan the various existing packaging formats.

## Enforce your CI results!

Running CIs, by itself, does not improve the security of your project. You need to:

- Effectively write tests for your code: without new tests, you won't find new bugs... Use code coverage! 
- Take into account the CI results: open issues, create PRs, fix the errors and warnings!
- Enforce successful pipelines to add code to your project with the configuration of your branches, releases and merge requests.

## GitLab security panel

GitLab is able to regroup all the data regarding your CI security tests into a managing panel. This is very useful to have an overview of your testing and see what can be improved.

Sadly, the full GitLab security panel is not available to the free tier, but can nevertheless be useful to free users.

# ... and security for CI

## How can CI be dangerous?

Basically, CI is using a server to **execute code**. If the CI flow is compromised, attackers can use it to gain control of the CI servers and launch various attacks.

The attacks can be targeting your own code, or serve as a proxy to attack other targets.

## Supply chain security

Supply chain security is working on **securing all the dependencies and external tools** you will use to create your application. 

This means you must be aware of all the potential security vulnerabilities in the software you use, be it directly in your application or in tools in your developing workflow.

## Exploiting CI

Be aware that any people that have write access to your CI configuration can take control of your CI runners, and launch various attacks from there. For example :

::: nonincremental
- Leaking secrets
- Attack private projects in shared runners
- Insert vulnerabilities at build time
- Falsify the CI results
:::

## Prevent CI attacks

In order to prevent CI attacks, you must control as precisely as possible who have access to CI configuration and harden your CI server. Meaning :

- Only the people allowed to modify the CI must be able to do so should be able to do it. Do not run any CI job on untrusted configurations.
- This people must have reinforced security overview, e.g. enforcing 2FA, special monitoring, etc.
- The CI servers must be hardened so that it is difficult to execute suspicious code on it (time limits, isolation, resources restrictions) and must be separated for sensible projects (beware of shared runners!)

# Conclusion

## On CI

CI is really a **powerful tool to enhance the security of your applications** and must be used as much as possible, especially with new languages such as Rust that have tools within the language to test, lint or fuzz your code. However, the amount of CI you do must take into account the resource and time cost.

As every new technology, CI brings with it its lot of **new attack vectors**. Never forget that CI is essentially giving access to a dedicated server and that you must take defensive measures accordingly.

## Going further : Continuous Deployment 

Continuous Delivery (CD) is the practice of **automatically (re)deploying applications in diverse environments** once compiled and tested. CD is often coming alongside CI (we talk about CI/CD) as they are similar in theory (doing things after code is made available), but the setup of a CD workflow and its security differs a lot from CI in practice.

A few examples of security struggles : analyzing the security of IaC, checking the security of deployment configurations, securing access to testing / production servers, management of secrets, validation workflow, malicious updates... 

## The stage is yours

Questions? Remarks?
