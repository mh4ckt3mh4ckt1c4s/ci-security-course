# CI security course

This repository contains the code needed to generate the slides I used for teaching the CI security course I gave at Télécom SudParis, along with the slides themselves.

## Using this repository

The slides are available in the `output` directory ([here](output/slides.pdf)). These slides are written using my [ultimate pandoc framework](https://gitlab.com/mh4ckt3mh4ckt1c4s/ultimate-pandoc-framework) project. Refer to this project to understand how this repository is organized.

This course was given alongside demonstrations made on this [example project](https://gitlab.com/mh4ckt3mh4ckt1c4s/gitlab-ci-rust-example).

## License

The content of the course is under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license. See the [LICENSE](LICENSE) for more information. The framework used for generating these slides is under the MIT license as specified inside [this repository](https://gitlab.com/mh4ckt3mh4ckt1c4s/ultimate-pandoc-framework).